<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Post;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DatabaseTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_db()
    {
        \App\Models\User::factory(1)->create();
        \App\Models\Post::factory(2)->create();

        $this->assertDatabaseHas('posts', [
            'id' => 1
        ]);

        $this->assertDatabaseHas('posts', [
            'id' => 2
        ]);
    }
}