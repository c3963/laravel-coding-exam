<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthRouteTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_auth_route_edit()
    {
        $post = Post::first();
        $response = $this->get(route('posts.edit', $post ? $post->id : 1));
        $response->assertRedirect('/login');
    }

    public function test_auth_route_update()
    {
        $user = User::create([
            'name' => Str::random(15),
            'email' => Str::random(20) . "@gmail.com",
            'email_verified_at' => now(),
            'password' => bcrypt('secret'), // password
            'remember_token' => Str::random(10),
        ]);
        $post = Post::create([
            'title' => 'sample for unit test initial',
            'body' => 'sample for unit test initial',
            'user_id' => $user->id
        ]);

        $post = Post::first();
        $post->update([
            'title' => 'sample for unit test',
            'body' => 'sample for unit test',
        ]);
        $response = $this->put(route('posts.update', $post ? $post->id : 1));
        $response->assertRedirect('/login');
    }
    
    public function test_auth_route_delete()
    {
        $user = User::create([
            'name' => Str::random(15),
            'email' => Str::random(20) . "@gmail.com",
            'email_verified_at' => now(),
            'password' => bcrypt('secret'), // password
            'remember_token' => Str::random(10),
        ]);

        $post = Post::create([
            'title' => 'sample for unit test initial',
            'body' => 'sample for unit test initial',
            'user_id' => $user->id
        ]);

        $post = Post::first();
        $response = $this->delete(route('posts.destroy', $post ? $post->id : 1));
        $response->assertRedirect('/login');
    }

    public function test_auth_route_store()
    {
        $params =[
            'title' => 'sample for unit test',
            'body' => 'sample for unit test',
        ];

        $response = $this->post(route('posts.store', $params));
        $response->assertRedirect('/login');
    }
}
