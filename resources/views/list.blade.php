<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title inertia>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    <!-- Scripts -->
    @routes
    <script src="{{ mix('js/app.js') }}" defer></script>
</head>

<body class="font-sans antialiased">
    <div id="app">
        <div>
            <!---->
            <div>
                <!--v-if-->
            </div>
            <div class=" min-h-screen bg-gray-100">
                <nav class="bg-white border-b border-gray-100">
                    <!-- Primary Navigation Menu -->
                    <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                        <div class="flex justify-between h-16">
                            <div class="flex">
                                <!-- Logo -->
                                <div class="flex-shrink-0 flex items-center"><a href="http://localhost:8000"><svg
                                            viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"
                                            class="block h-9 w-auto">
                                            <path
                                                d="M11.395 44.428C4.557 40.198 0 32.632 0 24 0 10.745 10.745 0 24 0a23.891 23.891 0 0113.997 4.502c-.2 17.907-11.097 33.245-26.602 39.926z"
                                                fill="#6875F5"></path>
                                            <path
                                                d="M14.134 45.885A23.914 23.914 0 0024 48c13.255 0 24-10.745 24-24 0-3.516-.756-6.856-2.115-9.866-4.659 15.143-16.608 27.092-31.75 31.751z"
                                                fill="#6875F5"></path>
                                        </svg></a></div><!-- Navigation Links -->
                                <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex"><a
                                        class="inline-flex items-center px-1 pt-1 border-b-2 border-indigo-400 text-sm font-medium leading-5 text-gray-900 focus:outline-none focus:border-indigo-700 transition"
                                        href="http://localhost:8000"> Post List </a></div>
                            </div>
                            <div class="hidden sm:flex sm:items-center sm:ml-6">
                                <div class="ml-3 relative">
                                    <div class="relative">
                                        <div>
                                            @if (auth()->user())
                                            <span class="inline-flex rounded-md"><button type="button"
                                                    class="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-gray-500 bg-white hover:text-gray-700 focus:outline-none transition">Wilson
                                                    Sacdalan Santa Cruz </button>
                                            </span>
                                        </div>
                                        @else
                                        <span
                                            class="inline-flex rounded-md px-6 py-4 whitespace-nowrap text-center text-sm font-medium text-indigo-600 hover:text-indigo-900">
                                            <a class="px-4" href="{{ route('login') }}">Login</a> |
                                            <a class="px-4" href="{{ route('register') }}">Register</a>
                                        </span>
                                        @endif
                                        <!-- Full Screen Dropdown Overlay -->
                                        <div class="fixed inset-0 z-40" style="display: none;"></div>
                                    </div>
                                </div>

                            </div><!-- Hamburger -->
                            <div class="-mr-2 flex items-center sm:hidden"><button
                                    class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition"><svg
                                        class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                                        <path class="inline-flex" stroke-linecap="round" stroke-linejoin="round"
                                            stroke-width="2" d="M4 6h16M4 12h16M4 18h16"></path>
                                        <path class="hidden" stroke-linecap="round" stroke-linejoin="round"
                                            stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
                                    </svg></button></div>
                        </div>
                    </div><!-- Responsive Navigation Menu -->
                    <div class="hidden sm:hidden">
                        <div class="pt-2 pb-3 space-y-1">
                            <div><a class="block pl-3 pr-4 py-2 border-l-4 border-indigo-400 text-base font-medium text-indigo-700 bg-indigo-50 focus:outline-none focus:text-indigo-800 focus:bg-indigo-100 focus:border-indigo-700 transition"
                                    href="http://localhost:8000/Post List"> Post List </a></div>
                        </div><!-- Responsive Settings Options -->

                    </div>
                </nav><!-- Page Heading -->
                <header class="bg-white shadow">
                    <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                        <h2 class="font-semibold text-xl text-gray-800 leading-tight"> Post List </h2>
                    </div>
                </header><!-- Page Content -->
                <main>
                    <div class="py-12">
                        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                            <div class="p-6 sm:px-20 bg-white border-b border-gray-200">
                                @include('post_list', $posts)
                            </div>
                        </div>
                    </div>
            </div>
            </main>
        </div>
    </div>
    </div>

    <script src="http://localhost:3000/browser-sync/browser-sync-client.js"></script>

    @if (Route::currentRouteName() !== 'list')
    @inertia
    @endif

</body>


</html>